#!/bin/bash

b_date=$(date +%Y%m%d-%H%M%S)
b_logdir=/home/${USER}/logs/vmbackup
b_last=${b_logdir}/lastbackup
b_disk=/dev/sdb1
b_luksdst=sdb1
b_mountp=/mnt/backup
b_src=/
b_dst=${b_mountp}/root
b_excl=/etc/sysbackup-excl.conf
b_snapdst=${b_mountp}/snapshot

if [[ "${1}" != "run" ]] ; then
	opt1="--dry-run"
	opt2="_DRY"
	echo "This is a DRY RUN, use \`$(basename ${0}) run' to backup for real."
fi

b_log=${b_logdir}/vmbackup_${b_date}${opt2}.log

sudo cryptsetup luksOpen ${b_disk} ${b_luksdst}
sudo mount -o compress=lzo,subvolid=5 /dev/mapper/${b_luksdst} ${b_mountp}

if [ ! "$(mount | grep "on ${b_mountp} ")" ] ; then
	echo "Abort."
	exit 1
fi

{ time sudo rsync ${opt1} -ax --delete --exclude-from=${b_excl} --info=all2,progress0 ${b_src} ${b_dst}/; } 1> ${b_log} 2>&1
gzip ${b_log}

if [[ "${1}" = "run" ]] ; then
	touch ${b_last}
	sudo btrfs subvolume snapshot -r ${b_dst} ${b_snapdst}/${b_date}.snap
fi

sudo btrfs filesystem usage -T -h ${b_mountp}

sudo umount ${b_mountp}
sudo cryptsetup luksClose ${b_luksdst}

