#!/bin/bash
#
# Crypted File System - Mounter Unmounter
#
# --- ABOUT ---
# This script is used to mount and unmount file systems crypted with cryptsetup
# with the luks method.
# For more info about creating crypted fs with luks, read the cryptsetup manual.
#
# The syntax of this script is very basic. If you want to improve it, feel free
# to do it. Let me know your patch!
#
# --- USAGE ---
# Before you can use this script, you need a luks crypted device, a mount point
# and root privileges.
#
# To mount a crypted fs:
#
# 	cfs-mu mount [device] [node_name]
#
# Note: [node_name] must have the same name of the mount point in ${mntpath}
#
# To unmount a mounted crypted fs:
# 	cfs-mu umount [node_name]

mntpath="/mnt"

if [[ ${UID} != 0 ]]; then
	sudocmd="sudo"
fi

case $1 in
        mount)
                echo -n "Verifying device presence...		"
                if [ -b ${2} ] ; then
                    echo "OK"
                else
                    echo "I can't find the device."
                    exit 127
                fi

                echo -n "Verifying mountpoint presence...	"
                if [ -d ${mntpath}/${3} ]; then
		    echo "OK"
		else
		    echo -n "Mountpoint doesn't exist. Create it in ${mntpath}/, "
		    echo "with the same name of node."
		    exit 127
		fi

                if [ $? = 0 ]; then ${sudocmd} cryptsetup luksOpen ${2} ${3}
                fi

                if [ $? = 0 ]; then
		    ${sudocmd} mount /dev/mapper/${3} ${mntpath}/${3}
		    echo "Device mounted in ${mntpath}/${3}"
		fi

                if [ $? != 0 ]; then
                    echo "Can't mount the file system."
                    ${sudocmd} cryptsetup luksClose ${3}
                    exit 1
                fi
        ;;
        umount)
                ${sudocmd} umount /dev/mapper/${2}
		if [ $? = 0 ]; then
                    echo "File system unmounted."
                fi
                if [ $? != 0 ]; then
                    echo "Can't unmount the file system."
                    exit 1
                fi

                ${sudocmd} cryptsetup luksClose ${2}
                if [ $? = 0 ]; then
                    echo "Node unmapped."
                else
		    echo "Can't unmap the node."
		    exit 1
                fi
        ;;
        *)
        echo "Crypted File System - Mounter Unmounter"
        echo "  usage:  	cfs-mu mount [device] [node_name]"
        echo "  		cfs-mu umount [node_name]"
        exit 1
        ;;
esac
