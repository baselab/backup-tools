#!/bin/bash

b_logdir=/home/${LOGNAME}/logs/vmbackup
b_last=${b_logdir}/lastbackup
b_lag=$(($(date +%s) - $(stat -c %Y "${b_last}")))
b_maxlag=260000

b_lastdate=$(stat -c %y "${b_last}" | cut -d " " -f 1)
b_lasttime=$(stat -c %y "${b_last}" | cut -d " " -f 2 | cut -d "." -f 1)

rcptto="${1}"

if [ "${b_lag}" -ge "${b_maxlag}" ] ; then
    cat <<EOF | mail -s Backup "${rcptto}"

Host:           ${HOSTNAME}
Last backup:    ${b_lastdate} - ${b_lasttime}

Please make a backup as soon as possible.

EOF
fi
